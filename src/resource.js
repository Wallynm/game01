var res = {
    HelloWorld_png : "res/HelloWorld.png",
    spineboy_atlas: "res/spine/spineboy.atlas",
    spineboy_json: "res/spine/spineboy.json",
    spineboy_png: "res/spine/spineboy.png",
    sprite_png: "res/spine/sprite.png",
    goblins_png: "res/spine/goblins-ffd.png",
    goblins_atlas: "res/spine/goblins-ffd.atlas",
    goblins_json: "res/spine/goblins-ffd.json"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}

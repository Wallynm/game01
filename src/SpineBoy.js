var sp = sp || {};

var SpineBoy = cc.Layer.extend({
    _spineboy:null,
    _debugMode: 0,
    _flipped: false,
    _idx:0,
    ctor:function (idx) {
        console.log('213')
        this._super(cc.color(0,0,0,0), cc.color(98,99,117,255));
        this._idx = idx;
        var size = cc.director.getWinSize();

        /////////////////////////////
        // Make Spine's Animated skeleton Node
        // You need 'json + atlas + image' resource files to make it.
        // No JS binding for spine-c in this version. So, only file loading is supported.
        var spineBoy;

        if (idx % 2 == 0) {
            spineBoy = new customSkeletonAnimation(res.spineboy_json, res.spineboy_atlas);
        } 
        else {
            spineBoy = new sp.SkeletonAnimation(res.spineboy_json, res.spineboy_atlas);
        }



        spineBoy.setPosition(cc.p(size.width / 2, size.height / 2 - 150));
        spineBoy.setMix('walk', 'jump', 0.2);
        spineBoy.setMix('jump', 'run', 0.2);
        spineBoy.setAnimation(0, 'walk', true);
        //spineBoy.setAnimationListener(this, this.animationStateEvent);
        spineBoy.setScale(0.5);
        this.addChild(spineBoy, 4);





        this._spineboy = spineBoy;
        spineBoy.setStartListener(function(trackIndex){
            var entry = spineBoy.getState().getCurrent(trackIndex);
            if(entry){
                var animationName = entry.animation ? entry.animation.name : "";
                cc.log("%d start: %s", trackIndex, animationName);
            }
        });
        spineBoy.setEndListener(function(traceIndex){
            cc.log("%d end.", traceIndex);
        });
        spineBoy.setCompleteListener(function(traceIndex, loopCount){
            cc.log("%d complete: %d", traceIndex, loopCount);
        });
        spineBoy.setEventListener(function(traceIndex, event){
            cc.log( traceIndex + " event: %s, %d, %f, %s",event.data.name, event.intValue, event.floatValue, event.stringValue);
        });

        var jumpEntry = spineBoy.addAnimation(0, "jump", false, 3);
        spineBoy.addAnimation(0, "run", true);
        // spineBoy.setTrackStartListener(jumpEntry, function(traceIndex){
        //     cc.log("jumped!");
        // });

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ALL_AT_ONCE,
            onTouchesBegan: function(touches, event){
                if(spineBoy.getTimeScale() === 1.0)
                    spineBoy.setTimeScale(0.3);
                else
                    spineBoy.setTimeScale(1);
            }
        }, this);

        cc.MenuItemFont.setFontSize(20);
        var bonesToggle = new cc.MenuItemToggle(
            new cc.MenuItemFont("Debug Bones: Off"),
            new cc.MenuItemFont("Debug Bones: On"));
        bonesToggle.setCallback(this.onDebugBones, this);
        bonesToggle.setPosition(160 - size.width / 2, 120 - size.height / 2);
        var slotsToggle = new cc.MenuItemToggle(
            new cc.MenuItemFont("Debug Slots: Off"),
            new cc.MenuItemFont("Debug Slots: On"));
        slotsToggle.setCallback(this.onDebugSlots, this);
        slotsToggle.setPosition(160 - size.width / 2, 160 - size.height / 2);
        var menu = new cc.Menu();
        menu.ignoreAnchorPointForPosition(true);
        menu.addChild(bonesToggle);
        menu.addChild(slotsToggle);
        this.addChild(menu, 5);
    },

    onDebugBones: function(sender){
        this._spineboy.setDebugBonesEnabled(!this._spineboy.getDebugBonesEnabled());
    },

    onDebugSlots: function(sender){
        this._spineboy.setDebugSlotsEnabled(!this._spineboy.getDebugSlotsEnabled());
    },

    subtitle:function () {
        if (this._idx % 2 == 0) {
            return "custom spine test";
        }
        else {
            return "Spine test";
        }
        
    },
    title:function () {
        return "Spine test";
    },

    animationStateEvent: function(obj, trackIndex, type, event, loopCount) {
        var entry = this._spineboy.getCurrent();
        var animationName = (entry && entry.animation) ? entry.animation.name : 0;
        switch(type) {
            case sp.ANIMATION_EVENT_TYPE.START:
                cc.log(trackIndex + " start: " + animationName);
                break;
            case sp.ANIMATION_EVENT_TYPE.END:
                cc.log(trackIndex + " end:" + animationName);
                break;
            case sp.ANIMATION_EVENT_TYPE.EVENT:
                cc.log(trackIndex + " event: " + animationName);
                break;
            case sp.ANIMATION_EVENT_TYPE.COMPLETE:
                cc.log(trackIndex + " complete: " + animationName + "," + loopCount);
                if(this._flipped){
                    this._flipped = false;
                    this._spineboy.setScaleX(0.5);
                }else{
                    this._flipped = true;
                    this._spineboy.setScaleX(-0.5);
                }
                break;
            default :
                break;
        }
    },
    
    // automation
    numberOfPendingTests:function() {
        return 1;
    },
    getTestNumber:function() {
        return 0;
    }
});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var spineBoy = new SpineBoy();
        this.addChild(spineBoy);
    }
});